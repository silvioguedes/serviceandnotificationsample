package com.silvioapps.serviceandnotificationsample;

import android.os.Binder;

/**
 * Created by Silvio Guedes on 02/07/2016.
 */
public class MyBinder extends Binder {
    private MyService service = null;

    public MyBinder(MyService service){
        this.service = service;
    }

    public MyService getService(){
        return service;
    }
}
