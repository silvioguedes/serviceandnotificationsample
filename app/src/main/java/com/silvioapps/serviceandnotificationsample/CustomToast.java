package com.silvioapps.serviceandnotificationsample;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

/**
 * Created by Silvio Guedes on 04/07/2016.
 */
public class CustomToast {
    private static Handler mHandler = new Handler();
    public static final long TEN_SECONDS = 10000;

    public static void showToastByHandler(final Context context, final CharSequence text, long sleep){
        try {
            Thread.sleep(sleep);
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    Toast.makeText(context,text,Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            Log.e("TAG", e.toString());
        }
    }
}
