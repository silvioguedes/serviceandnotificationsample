package com.silvioapps.serviceandnotificationsample;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

/**
 * Created by Silvio Guedes on 05/07/2016.
 */
public class MyNotification {
    public static final int SIMPLE_NOTIFICATION_ID = 0;
    public static final String ACTION_INTENT = "ACTION INTENT";
    private PendingIntent pendingIntent = null;
    private PendingIntent actionIntent = null;
    private Uri sound = null;
    private int lightsOn = 1000;
    private int lightsOff = 5000;
    private long[] vibratePattern = {100,200,300};
    private Bitmap bitmap = null;
    private Intent intent = null;
    private Context context = null;
    private Notification notification = null;
    private NotificationManagerCompat notificationManagerCompat = null;

    private static Handler mHandler = new Handler();
    public static final long TEN_SECONDS = 10000;

    public MyNotification(Intent intent, Context context){
        this.intent = intent;
        this.context = context;
    }

    public boolean isAndroidVersionJellyBeanOrNewer(){
        int currentApiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.JELLY_BEAN) {
            return true;
        }

        return false;
    }

    public void createNotification(){
        if(isAndroidVersionJellyBeanOrNewer()){
            TaskStackBuilder taskStackBuilder = TaskStackBuilder.create(context);
            taskStackBuilder.addParentStack(PendingIntentActivity.class);
            taskStackBuilder.addNextIntent(intent);

            pendingIntent = taskStackBuilder.getPendingIntent(SIMPLE_NOTIFICATION_ID, PendingIntent.FLAG_UPDATE_CURRENT);
            actionIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_INTENT), 0);
        }

        String soundFile = "tone";//"sound";
        sound = Uri.parse("android.resource://"+context.getPackageName()+"/raw/"+soundFile);

        bitmap = BitmapFactory.decodeResource(context.getResources(),R.drawable.large_icon);

        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
        inboxStyle.setBigContentTitle("BIG CONTENT TITLE");
        inboxStyle.setSummaryText("SUMMARY TEXT");
        inboxStyle.addLine("LINE 1");
        inboxStyle.addLine("LINE 2");
        inboxStyle.addLine("LINE 3");

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentTitle("CONTENT TITLE");
        builder.setContentText("CONTENT TEXT");
        builder.setTicker("TICKER");
        builder.setWhen(System.currentTimeMillis());
        builder.setAutoCancel(true);
        builder.setOngoing(false);
        builder.setLights(Color.RED,lightsOn,lightsOff);
        builder.setSound(sound);
        builder.setVibrate(vibratePattern);

        if(isAndroidVersionJellyBeanOrNewer()) {
            builder.setContentIntent(pendingIntent);
            builder.setDeleteIntent(pendingIntent);
            builder.addAction(R.drawable.red_icon, "ACTION INTENT", actionIntent);
        }

        builder.setContentInfo("CONTENT INFO");
        builder.setNumber(123);
        builder.setSubText("SUBTEXT");
        //builder.setLargeIcon(bitmap);//TODO ver tamanho do bitmap para não gerar erro de pouca memória
        builder.setStyle(inboxStyle);

        notificationManagerCompat = NotificationManagerCompat.from(context);
        notification = builder.build();
    }

    public void startNotification(long sleep){
        if(mHandler != null && context != null && notificationManagerCompat != null && notification != null) {
            try {
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        notificationManagerCompat.notify(SIMPLE_NOTIFICATION_ID, notification);
                    }
                });

                Thread.sleep(sleep);
            } catch (Exception e) {
                Log.e("TAG", e.toString());
            }
        }
    }
}
