package com.silvioapps.serviceandnotificationsample;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MyBroadcastReceiverForService extends BroadcastReceiver {
    public MyBroadcastReceiverForService() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Intent newIntent = new Intent(context,MyService.class);
        newIntent.putExtra(MyService.STOP_THREAD,false);
        context.startService(newIntent);
    }
}
