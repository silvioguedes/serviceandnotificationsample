package com.silvioapps.serviceandnotificationsample;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {
    private boolean stopThread = false;
    public static final String STOP_THREAD = "stopThread";
    private Thread thread = null;

    private MyNotification notification = null;

    public MyService() {

    }

    @Override
    public void onCreate(){
        super.onCreate();

        CustomToast.showToastByHandler(getApplicationContext(),"SERVICE STARTED",0);

        stopThread = false;

        Intent intent = new Intent(this, PendingIntentActivity.class);
        notification = new MyNotification(intent,getApplicationContext());
        notification.createNotification();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startID) {
        if(intent != null) {
            stopThread = intent.getBooleanExtra(STOP_THREAD,false);

            thread = new Thread(){
                public void run(){
                    while(stopThread == false){
                        if(notification != null){
                            notification.startNotification(MyNotification.TEN_SECONDS);
                        }
                    }

                    if(stopThread) {
                        //TODO
                    }
                }
            };
            thread.start();
        }

        return super.onStartCommand(intent,flags,startID);
    }

    @Override
    public IBinder onBind(Intent intent) {
        if(intent != null) {
            CustomToast.showToastByHandler(getApplicationContext(),"SERVICE BOUND",0);
        }

        MyBinder binder = new MyBinder(this);

        return binder;
    }

    @Override
    public void onDestroy(){
        stopThread = true;

        if(thread != null) {
            thread = null;
        }

        CustomToast.showToastByHandler(getApplicationContext(),"SERVICE STOPPED",0);

        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        CustomToast.showToastByHandler(getApplicationContext(),"SERVICE UNBOUND",0);

        return false;
    }
}
