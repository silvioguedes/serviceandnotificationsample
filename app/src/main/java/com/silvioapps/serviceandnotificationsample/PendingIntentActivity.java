package com.silvioapps.serviceandnotificationsample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class PendingIntentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_intent);
    }
}
