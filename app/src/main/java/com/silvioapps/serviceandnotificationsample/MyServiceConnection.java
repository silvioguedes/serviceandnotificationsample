package com.silvioapps.serviceandnotificationsample;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by Silvio Guedes on 02/07/2016.
 */
public class MyServiceConnection implements ServiceConnection {
    private MyService myService = null;
    private boolean isBound = false;

    @Override
    public void onServiceConnected(ComponentName componentName, IBinder service){
        isBound = true;

        MyBinder myBinder = (MyBinder)service;
        myService = myBinder.getService();

        if(myService != null) {
            //TODO
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName){
        isBound = false;
    }

    public boolean isBound(){
        return isBound;
    }
}
