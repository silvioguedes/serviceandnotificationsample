package com.silvioapps.serviceandnotificationsample;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private Button startServiceButton = null;
    private Button stopServiceButton = null;

    private MyServiceConnection serviceConnection = null;
    private Intent serviceIntent = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        serviceIntent = new Intent(MainActivity.this,MyService.class);
        serviceConnection = new MyServiceConnection();

        startServiceButton = (Button)findViewById(R.id.startService);
        startServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                serviceIntent.putExtra(MyService.STOP_THREAD,false);
                startService(serviceIntent);
            }
        });

        stopServiceButton = (Button)findViewById(R.id.stopService);
        stopServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(serviceIntent);
            }
        });
    }

    @Override
    public void onResume() {
        if(serviceIntent != null && serviceConnection != null) {
            bindService(serviceIntent, serviceConnection, 0);
        }

        super.onResume();
    }

    @Override
    protected void onPause() {
        if (serviceConnection != null) {
            unbindService(serviceConnection);
        }

        super.onPause();
    }
}
